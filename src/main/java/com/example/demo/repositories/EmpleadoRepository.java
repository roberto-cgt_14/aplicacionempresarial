package com.example.demo.repositories;

import java.util.ArrayList;

import com.example.demo.models.EmpleadoModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpleadoRepository extends CrudRepository<EmpleadoModel, Long> {

    public abstract ArrayList<EmpleadoModel> findByPuesto(String puesto);
}