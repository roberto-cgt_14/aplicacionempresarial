package com.example.demo.services;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Optional;

import com.example.demo.models.EmpleadoModel;
import com.example.demo.repositories.EmpleadoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class EmpleadoService {
    @Autowired
    EmpleadoRepository empleadoRepository;

    public ArrayList<EmpleadoModel> obtenerEmpleados() {
        return (ArrayList<EmpleadoModel>) empleadoRepository.findAll();
    }

    public EmpleadoModel guardarEmpleado(String puesto, String nombre, MultipartFile foto, String brm) {
        EmpleadoModel empleado = new EmpleadoModel();

        String fotoName = StringUtils.cleanPath(foto.getOriginalFilename());
        if (fotoName.contains("..")) {
            System.out.println("------------ File invalid Contains----------");

        }
        try {

            empleado.setFoto(Base64.getEncoder().encodeToString(foto.getBytes()));
        } catch (Exception e) {
            System.out.println(" -----------------File invalid -------------");
        }

        empleado.setBrm(brm);
        empleado.setNombre(nombre);
        empleado.setPuesto(puesto);

        return empleadoRepository.save(empleado);
    }

    public boolean editarEmpleado(Long id, String puesto, String nombre, MultipartFile foto, String brm) {
        try {
            EmpleadoModel empleadoFromDb = empleadoRepository.findById(id).get();
            empleadoFromDb.setNombre(nombre);
            empleadoFromDb.setBrm(brm);
            empleadoFromDb.setPuesto(puesto);
            try {

                empleadoFromDb.setFoto(Base64.getEncoder().encodeToString(foto.getBytes()));
            } catch (Exception e) {
                System.out.println(" -----------------File invalid -------------");
            }
            empleadoRepository.save(empleadoFromDb);

            return true;
        } catch (Exception err) {
            return false;
        }
    }

    public Optional<EmpleadoModel> obtenerPorId(Long id) {
        return empleadoRepository.findById(id);
    }

    public boolean eliminarEmpleado(Long id) {
        try {
            empleadoRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    /*
     * public EmpleadoModel eliminarEmpleado(){
     * 
     * }
     */

}