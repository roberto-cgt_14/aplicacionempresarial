package com.example.demo.controllers;

import java.util.ArrayList;
import java.util.Optional;

import com.example.demo.models.EmpleadoModel;
import com.example.demo.services.EmpleadoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/empleados")
public class EmpleadoController {
    @Autowired
    EmpleadoService empleadoService;

    @GetMapping()
    public ArrayList<EmpleadoModel> obtenerEmpleados() {
        return empleadoService.obtenerEmpleados();
    }

    @PostMapping()
    public EmpleadoModel guardarEmpleado(@RequestParam("nombre") String nombre, @RequestParam("puesto") String puesto,
            @RequestParam("brm") String brm,

            @RequestParam("foto") MultipartFile foto) {

        return this.empleadoService.guardarEmpleado(puesto, nombre, foto, brm);
    }

    @PutMapping(path = "/{id}")
    public String editarEmpleado(@PathVariable("id") Long id, @RequestParam("nombre") String nombre,
            @RequestParam("puesto") String puesto,
            @RequestParam("brm") String brm,

            @RequestParam("foto") MultipartFile foto) {
        boolean ok = this.empleadoService.editarEmpleado(id, puesto, nombre, foto, brm);
        if (ok) {
            return "Se actualizó el empleado con el id " + id;
        } else {
            return "No se pudo actualizar el empleado con el id " + id;
        }
    }

    @GetMapping(path = "/{id}")
    public Optional<EmpleadoModel> obtenerEmpleadoPorId(@PathVariable("id") Long id) {
        return this.empleadoService.obtenerPorId(id);
    }

    @DeleteMapping(path = "/{id}")
    public String eliminarById(@PathVariable("id") Long id) {
        boolean ok = this.empleadoService.eliminarEmpleado(id);
        if (ok) {
            return "Se eliminó el empleado con id" + id;
        } else {
            return "No se pudo eliminar el empleado con id" + id;
        }
    }

}
